var path = require("path");
var PackageJson = require(path.resolve('./package.json'));
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var wdsConfig = PackageJson.config.wds;

var env = process.env.NODE_ENV || "production";

console.log('--------------------------');
console.log(PackageJson.name + ' (v' + PackageJson.version + ')');
console.log(env + ' build');
console.log('--------------------------');
process.noDeprecation = true;

var config = {
	entry: {
		index: [path.resolve('./src/layouts/index/index.js')]
	},
	output: {
		path: path.resolve('dist'),
		filename: 'assets/js/[name].js',
		publicPath: '/'
	},
	module: {
		rules: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader',
			query: {
				presets: [require('babel-preset-es2015')]
			}
		}, {
			test: /\.scss$/,
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: ['css-loader', 'sass-loader'],
				disable: (env === 'development')
			})
		}/* {
			test: /\.(css|scss)$/,
			loader: ExtractTextPlugin.extract({
				fallbackLoader: "style-loader",
				loader: "css-loader!sass-loader",
			}),
		}*/]
	},
	resolve: {
		alias: {
			src: path.resolve(__dirname, 'src'),
			assets: path.resolve(__dirname, 'src/assets'),
		}
	},
	plugins: [
		new webpack.DefinePlugin({
			__PACKAGEJSON__: JSON.stringify(PackageJson),
			__ENV__: JSON.stringify(env)
		}),
		new ExtractTextPlugin({
			filename: 'assets/css/[name].css',
			allChunks: true,
			disable: (env === 'development')
		})
	]
}

if (env === 'development') {
	config.devtool = "inline-source-map";
	config.devServer = {
		contentBase: path.resolve("dist"),
		https: wdsConfig.https,
		port: wdsConfig.port
	}
} else {
	config.plugins.push(
		new webpack.optimize.UglifyJsPlugin()
	)
}

module.exports = config;