
var path = require("path");

module.exports = {
	title: "Static site",
	description: "Static site description",
	keywords: "Static, Site, Keywords",
	url: "http://site.com/",
	require: require,
	pathResolve: path.resolve
}